# HSH Requests Web Application #

HSH Requests Web Application - Documentaion & other related files Repository

### What is this repository for? ###

* Stores all the information related to this application.
* Including: Documentation, Diagrams, and more.
* HSHR Version 3.0

### How do I get set up? ###

* Clone HSHR-UI repository
* Clone HSHR-Backend repository
* (OPTIONAL) Clone this repository for documentation and other application related files
* Install Node.js, JDK v16, Angular, IDE of choise (VSCode, Eclipse, etc.), & PostgreSQL.
* If you are using VSCode add the Spring Extention Pack & Java Extention Pack
* Copy your postgre server information to the HSHR-Backend's application settings and change the first line to say Create instead of None.
* Run both HSHR-UI and HSHR-Backend together
* After first run change HSHR-Backend's application settings first line back to None.
